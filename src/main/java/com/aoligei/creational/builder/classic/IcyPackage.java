package com.aoligei.creational.builder.classic;

/**
 * 套餐‘冰爽’
 *
 * @author coder
 * @date 2022-05-30 17:45:30
 * @since 1.0.0
 */
public class IcyPackage extends AbstractPackage {
    @Override
    public void setName() {
        super.name = "冰爽";
    }
}
