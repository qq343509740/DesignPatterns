package com.aoligei.creational.builder.classic;

/**
 * 套餐‘烈焰’
 *
 * @author coder
 * @date 2022-05-30 11:11:26
 * @since 1.0.0
 */
public class FlamesPackage extends AbstractPackage {

    @Override
    public void setName() {
        super.name = "烈焰";
    }

}
