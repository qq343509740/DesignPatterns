package com.aoligei.behavioral.strategy;

/**
 * 目的地防疫政策
 *
 * @author coder
 * @date 2022-06-14 14:40:05
 * @since 1.0.0
 */
public interface EpidemicPreventionOfDestStrategy {

    /**
     * 疫情防控政策
     */
    void prevent();

}
