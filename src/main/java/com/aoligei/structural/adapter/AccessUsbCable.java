package com.aoligei.structural.adapter;

/**
 * 接入数据线
 *
 * @author coder
 * @date 2022-06-01 17:26:07
 * @since 1.0.0
 */
public interface AccessUsbCable {
    /**
     * 接入数据线
     */
    void access();
}
